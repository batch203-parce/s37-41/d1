const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers");
const auth = require("../auth");

// Router for creating a course
router.post("/", auth.verify, courseControllers.addCourse);

//Routing for viewing all courses (Admin only)
router.get("/all", auth.verify, courseControllers.getAllCourses);

// Router for viewing all active courses (Users only)
router.get("/", courseControllers.getAllActive);

// Route for viewing a specific course
router.get("/:courseId", courseControllers.getCourse);

// Route for updating a course
router.put("/:courseId", auth.verify, courseControllers.updateCourse);

// Route for archiving a course
router.patch("/archive/:courseId", auth.verify, courseControllers.archiveCourse);

module.exports = router;
