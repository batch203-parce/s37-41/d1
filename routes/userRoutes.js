const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");

const auth = require("../auth");

// Routes for checking email
router.post("/checkEmail", userControllers.checkEmailExists);

// Route for user registration
router.post("/register", userControllers.registerUser);

// Route for user login
router.post("/login", userControllers.logInUser);

// Router for user details
router.get("/details", auth.verify, userControllers.getProfile);

// Route for enrolling a user
router.post("/enroll", auth.verify, userControllers.enroll);

module.exports = router;