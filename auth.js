const jwt = require("jsonwebtoken");

// Used in the algorithm for encrypting our data which make it difficult to decode the information without the defined secret key.
const secret = "CourseBookingAPI";

// [SECTION] JSON Web Tokens

// Token Creation
/*
	Analogy:
		Pack the gift provided with a lock, which can only be open using the secret code as the key.
*/

// The "user" parameter will contain the values of the user upon login.
module.exports.createAccessToken = (user) => {

	console.log(user);
	
	// payload of the JWT
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	// Generate a JSON web token using the jwt's "sign method".
	// Syntax: 
		// jwt.sign(payload, secretOrPrivateKey, [options/callBackFunctions])

	return jwt.sign(data, secret, {});	
}


/*
- Analogy
	Receive the gift and open the lock to verify if the the sender is legitimate and the gift was not tampered with
*/

// Middleware function
module.exports.verify = (req, res, next) => {
	// The token is reetrieve from the request header.
	let token  = req.headers.authorization;
	//console.log(token);

	// If token is undefined, then req.headers.authorization is empty. Which means, the request did not pass a token in the authorization headers.
	if (token != undefined){
		//res.send({message: "Token received!"})

		// The token sent is a type of "Bearer" which when received contains the "Bearer" as a prefix to the string. To remove the "Bearer" prefix  we used the slice method to retrieve only the token.
		token = token.slice(7, token.length);
		console.log(token);

		// Validate the "token" using the "verify" method to decrypt the token using the secret code.
		// Syntax: jwt.verify(token, secretPrivateKey, [options/callBackFunction])

		return jwt.verify(token, secret, (err, data) => {
			// If JWT is not valid
			if (err) {
				return res.send({auth: "Invalid token!"})
			}
			// If JWT is valid
			else {
				next();
			}
		})
	}
	else {res.send({message: "Auth failed. No token provided!"})
	}
}

// Token decryption
/*
	Analogy
		Open the gift and get the content
*/
module.exports.decode = (token) => {
	if (token !== undefined) {
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			// if token is not valid
			if(err) {
				return null;
			}
			else {
				// decode method is used to obtain the information from the JWT
				// Syntax: jwt.decode(token, [options]);
				// Returns an object with access to the "payload" property which contains the user information stored when the token is generated.
				return jwt.decode(token, {complete:true}).payload;
			}
		})
	}
	else {
		// if token does not exist
		return null;
	}
}